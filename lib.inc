%define SYS_EXIT 60
%define STDOUT 1
%define SYS_WRITE 1
%define SYS_READ 0
%define STDIN 0

%define CHAR_NEWLINE 0x0A
%define CHAR_MINUS '-'
%define CHAR_SPACE 0x20
%define CHAR_ZERO '0'
%define CHAR_NULL 0x00
%define CHAR_TAB 0x09
section .text
 

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT ; exit syscall number
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку(rdi), возвращает её длину
string_length:
    xor rax,rax
    .loop:
        cmp byte[rdi+rax], 0
        je .next
        inc rax
        jmp .loop
    .next:
        ret

; Принимает указатель на нуль-терминированную строку(rdi), выводит её в stdout
; rsi - ссылка на начало строки, rdx - длина строки
print_string:
    xor rax, rax
    push rdi
    call string_length ; length now in rax
    pop rdi
    mov rsi, rdi
    mov rdx, rax
    mov rdi, STDOUT ; write to stdout
    mov rax, SYS_WRITE ; syscall write number
    syscall
    ret

; Принимает код символа и выводит его в stdout(rdi)
; rsi - ссылка на начало строки, rdx - длина строки
print_char:
    push di
    mov rax, SYS_WRITE  
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop di
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, CHAR_NEWLINE ; 0xA = '\n'
    jmp print_char
    

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    xor rdx, rdx
    mov rax, rdi
    mov rdi, rsp
    sub rsp, 21 ; 21 is bytes is enough to put a string and null terminated string(20 + 1)
    dec rdi
    mov rsi, 10
    mov byte[rdi], 0 ; 
    .loop:
        div rsi ; rax <- rax div 10, rdx <- rax mod 10
        dec rdi
        add dl, CHAR_ZERO ; decode
        mov [rdi], dl
        xor rdx, rdx
        cmp rax, 0 
        jne .loop
    .end:
        call print_string ;rdi - начало
        add rsp, 21
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    
    cmp rdi, 0
    jge .positive
    .negative:
        push rdi
        mov rdi, CHAR_MINUS
        call print_char
        pop rdi
        neg rdi
    .positive:
        jmp print_uint
        

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - ссылка на первую строку rsi - ссылка на вторюу строку
string_equals:
    xor rax, rax
    .loop:
        mov al, [rdi] ; Сохранение значения по ссылке rdi в аккумулятор, так как cmp не может работать с двумя пространствами в памяти
        cmp al, [rsi] 
        jne .set_zero_value ; Если ZF==0, то выход(если значения элементов не равны, то всё плохо
        cmp rax, CHAR_NULL ; проверка на нуль терминатор
        je .set_value ; Если ZF == 1, то есть строки закончились, то мы сделали всё что нужно, возвращаем 1
        inc rdi ; идём дальше по строке
        inc rsi
        xor rax,rax ; чисто на всякий случай
        jmp .loop
    .set_zero_value:
        xor rax, rax
        jmp .exit
    .set_value:
        mov rax,1
    .exit:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	mov	rax, SYS_READ	
	push	ax			
	mov	rdi, STDIN		
	mov	rsi, rsp		
	mov	rdx, 1			
	syscall				
	cmp	rax, 1			
	je	.success		
    .fail:					
        pop	ax			
        xor	rax, rax		
        ret				
    .success:				
        pop	ax			
        ret				


read_char_whitespaces:
    call read_char
    cmp rax, CHAR_NEWLINE
    je .whitespaces
    cmp rax, CHAR_SPACE
    je .whitespaces
    cmp rax, CHAR_TAB
    je .whitespaces
    ret
    .whitespaces:
        mov rax, -1
        ret


; Принимает: адрес начала буфера(rdi), размер буфера(rsi) 
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор


read_word:
	push	rdi			
	cmp	rsi, 1			
	jl	.buffer_error		
	push	rdi			
	push	rsi			
    .find_start:				
        call	read_char_whitespaces	
        cmp	rax, -1			
        je	.find_start		
        jmp	.after_read_char	
    .loop:					
        push	rdi			
        push	rsi			
        call	read_char_whitespaces	
    .after_read_char:			
        pop	rsi			
        pop	rdi			
        cmp	rax, -1			
        je	.stream_end		
        cmp	rax, CHAR_NULL		
        je	.stream_end		
        mov	[rdi], al		
        inc	rdi			
        dec	rsi			
        jz	.buffer_error		
        jmp	.loop			
    .stream_end:				
        cmp	rsi, 1			
        jge	.success
    .buffer_error:				
        pop	rax			
        xor	rax, rax		
        ret				
    .success:				
        mov	[rdi], byte CHAR_NULL
        pop	rax	
        mov	rdx, rdi
        sub	rdx, rax
        ret

 

; Принимает указатель на строку, пытается(rdi)
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax ; ans
    xor rsi, rsi ; current char
    xor r10, r10
    mov r10, 10
    xor r9, r9 ; length
    .loop:
        mov sil,[rdi] ; copy from mem(rdi) to sil(lower 8 bit of rsi)
        inc rdi ; rdi++ for next char in string
        test sil, sil ; check for null terminator
        je .end
        sub sil, CHAR_ZERO ; ascii code for numbers
        cmp rsi, 0 ; if current char-0<0 then currr char isn't number
        jl .end
        cmp rsi, 9 ; if current char - 9 > 0 then current char isn't number
        jg .end
        mul r10 ; * 10(decode current char)
        add rax, rsi ; rax = rax + rsi*10
        inc r9 ; r9++(length)
        jmp .loop
    .end:
        mov rdx, r9 ; length -> rdx
    ret




; Принимает указатель на строку(rdi), пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; Проверяем первый элемент на знак, если -, дописываем знак, иначе просто parse_uint
; Если parse int вернул 0, то там какой-то символ, проверяем его на знак
parse_int:
    xor rax, rax
    push rdi
    call parse_uint 
    pop rdi
    test rdx, rdx
    jne .parsed ; if zf = 0 then int is already parsed
    .check_char:
        mov al, [rdi]
        cmp al, CHAR_MINUS
        jne .not_int ; if ZF=0 then char isn't int
        inc rdi ; next char in string
        call parse_uint
        test rdx, rdx ; if ZF=1, then isn't number
        je .not_int
        inc rdx
        neg rax
    .parsed:
        ret
    .not_int:
        xor rdx, rdx
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi - указатель на строку
; rsi - указатель на буфер
; rdx - длина буфера
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length ; string length in rax
    inc rax ; cuz null terminator
    pop rdx
    pop rsi
    pop rdi
    cmp rdx, rax
    jl .fail ; if rdx<rax then end
    push rax
    .loop:
        mov r8b, [rdi]
        mov [rsi], r8b
        inc rsi
        inc rdi
        dec rax
        jne .loop
    pop rax
    jmp .end
    .fail:
        xor rax, rax
    .end:
        ret
